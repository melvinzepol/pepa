<?php 

if( ! function_exists( 'road_get_slider_setting' ) ) {
	function road_get_slider_setting() {
		return array(
			array(
				'type'        => 'dropdown',
				'holder'      => 'div',
				'heading'     => esc_html__( 'Style', 'jico' ),
				'param_name'  => 'style',
				'value'       => array(
					__( 'Grid view (default)', 'jico' )                    => 'product-grid',
					__( 'Grid view 2 (product bg 2 (see Demo 2))', 'jico' )   => 'product-grid-2',
					__( 'List view 1', 'jico' )                  => 'product-list',
					__( 'List view 2', 'jico' )                  => 'product-list-2',
					__( 'Grid view with countdown', 'jico' )     => 'product-grid-countdown',
				),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Enable slider', 'jico' ),
				'description' => __( 'If slider is enabled, the "column" ins General group is the number of rows ', 'jico' ),
				'param_name'  => 'enable_slider',
				'value'       => true,
				'save_always' => true, 
				'group'       => __( 'Slider Options', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'holder'     => 'div',
				'heading'    => __( 'Number of columns (screen: over 1500px)', 'jico' ),
				'param_name' => 'items_1500up',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '4', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 1200px - 1499px)', 'jico' ),
				'param_name' => 'items_1200_1499',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '4', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 992px - 1199px)', 'jico' ),
				'param_name' => 'items_992_1199',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '4', 'jico' ),
			), 
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 768px - 991px)', 'jico' ),
				'param_name' => 'items_768_991',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '3', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 640px - 767px)', 'jico' ),
				'param_name' => 'items_640_767',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '2', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: 480px - 639px)', 'jico' ),
				'param_name' => 'items_480_639',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '2', 'jico' ),
			),
			array(
				'type'       => 'textfield',
				'heading'    => __( 'Number of columns (screen: under 479px)', 'jico' ),
				'param_name' => 'items_0_479',
				'group'      => __( 'Slider Options', 'jico' ),
				'value'      => esc_html__( '1', 'jico' ),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Navigation', 'jico' ),
				'param_name'  => 'navigation',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'jico' ),
				'value'       => array(
					__( 'Yes', 'jico' ) => true,
					__( 'No', 'jico' )  => false,
				),
			),
			array(
				'type'        => 'dropdown',
				'heading'     => __( 'Pagination', 'jico' ),
				'param_name'  => 'pagination',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'jico' ),
				'value'       => array(
					__( 'No', 'jico' )  => false,
					__( 'Yes', 'jico' ) => true,
				),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Item Margin (unit: pixel)', 'jico' ),
				'param_name'  => 'item_margin',
				'value'       => 30,
				'save_always' => true,
				'group'       => __( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'textfield',
				'heading'     => __( 'Slider speed number (unit: second)', 'jico' ),
				'param_name'  => 'speed',
				'value'       => '500',
				'save_always' => true,
				'group'       => __( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider loop', 'jico' ),
				'param_name'  => 'loop',
				'value'       => true,
				'group'       => __( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'checkbox',
				'heading'     => __( 'Slider Auto', 'jico' ),
				'param_name'  => 'auto',
				'value'       => true,
				'group'       => __( 'Slider Options', 'jico' ),
			),
			array(
				'type'        => 'dropdown',
				'holder'      => 'div',
				'heading'     => esc_html__( 'Navigation style', 'jico' ),
				'param_name'  => 'navigation_style',
				'group'       => __( 'Slider Options', 'jico' ),
				'value'       => array(
					'Navigation center horizontal'	=> 'navigation-style1',
					'Navigation top right'	        => 'navigation-style2',
				),
			),
		);
	}
}
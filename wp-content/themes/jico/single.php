<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Jico_Theme
 * @since Jico 1.0
 */
$jico_opt = get_option( 'jico_opt' );
get_header();
$jico_bloglayout = 'sidebar';
if(isset($jico_opt['blog_layout']) && $jico_opt['blog_layout']!=''){
	$jico_bloglayout = $jico_opt['blog_layout'];
}
if(isset($_GET['layout']) && $_GET['layout']!=''){
	$jico_bloglayout = $_GET['layout'];
}
$jico_blogsidebar = 'right';
if(isset($jico_opt['sidebarblog_pos']) && $jico_opt['sidebarblog_pos']!=''){
	$jico_blogsidebar = $jico_opt['sidebarblog_pos'];
}
if(isset($_GET['sidebar']) && $_GET['sidebar']!=''){
	$jico_blogsidebar = $_GET['sidebar'];
}
if ( !is_active_sidebar( 'sidebar-1' ) )  {
	$jico_bloglayout = 'nosidebar';
}
switch($jico_bloglayout) {
	case 'nosidebar':
		$jico_blogclass = 'blog-nosidebar';
		$jico_blogcolclass = 12;
		$jico_blogsidebar = 'none';
		break;
	default:
		$jico_blogclass = 'blog-sidebar'; 
		$jico_blogcolclass = 9;
}
?>
<div class="main-container <?php if(isset($jico_opt['blog_banner']['url']) && ($jico_opt['blog_banner']['url'])!=''){ echo 'has-image';} ?>">
	<div class="title-breadcumbs">
		<!-- blog banner -->
		<?php if(isset($jico_opt['blog_banner']['url']) && ($jico_opt['blog_banner']['url'])!=''){ ?>
			<div class="blog-banner banner-image">
				<img src="<?php echo esc_url($jico_opt['blog_banner']['url']); ?>" alt="<?php esc_attr_e('Shop banner','jico') ?>" />
			</div>
		<?php } ?>
		<!-- end blog banner -->
		<div class="title-breadcumbs-text">
			<div class="container">
				<header class="entry-header">
					<h2 class="entry-title"><?php if(isset($jico_opt['blog_header_text']) && ($jico_opt['blog_header_text'] !='')) { echo esc_html($jico_opt['blog_header_text']); } else { esc_html_e('Blog', 'jico');}  ?></h2>
				</header>
				<div class="breadcrumb-container">
					<div class="container">
						<?php Jico_Class::jico_breadcrumb(); ?> 
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container">
		<div class="row">
			<div class="col-12 <?php echo 'col-lg-'.$jico_blogcolclass; ?>">
				<div class="page-content blog-page single <?php echo esc_attr($jico_blogclass); if($jico_blogsidebar=='left') {echo ' left-sidebar'; } if($jico_blogsidebar=='right') {echo ' right-sidebar'; } ?> ">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
						<?php comments_template( '', true ); ?>
					<?php endwhile; // end of the loop. ?>
				</div>
			</div>
			<?php
			$customsidebar = get_post_meta( $post->ID, '_jico_custom_sidebar', true );
			$customsidebar_pos = get_post_meta( $post->ID, '_jico_custom_sidebar_pos', true );
			if($customsidebar != ''){
				if($customsidebar_pos == 'left' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-12 col-lg-3 order-lg-last">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($jico_blogsidebar=='left') {
					get_sidebar();
				}
			} ?>
			<?php
			if($customsidebar != ''){
				if($customsidebar_pos == 'right' && is_active_sidebar( $customsidebar ) ) {
					echo '<div id="secondary" class="col-12 col-lg-3">';
						dynamic_sidebar( $customsidebar );
					echo '</div>';
				} 
			} else {
				if($jico_blogsidebar=='right') {
					get_sidebar();
				}
			} ?>
		</div>
	</div> 
	<!-- brand logo -->
	<?php 
		if(isset($jico_opt['inner_brand']) && function_exists('jico_brands_shortcode') && shortcode_exists( 'ourbrands' ) ){
			if($jico_opt['inner_brand'] && isset($jico_opt['brand_logos'][0]) && $jico_opt['brand_logos'][0]['thumb']!=null) { ?>
				<div class="inner-brands">
					<div class="container">
						<?php if(isset($jico_opt['inner_brand_title']) && $jico_opt['inner_brand_title']!=''){ ?>
							<div class="title">
								<h3><?php echo esc_html( $jico_opt['inner_brand_title'] ); ?></h3>
							</div>
						<?php } ?>
						<?php echo do_shortcode('[ourbrands]'); ?>
					</div>
				</div>
			<?php }
		}
	?>
	<!-- end brand logo --> 
</div>
<?php get_footer(); ?>